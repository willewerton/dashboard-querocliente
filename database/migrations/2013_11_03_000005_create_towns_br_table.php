<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTownsBrTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'towns_br';

    /**
     * Run the migrations.
     * @table towns_br
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('town', 45)->nullable();
            $table->boolean('principal_city')->default(0);
            $table->string('lat', 100)->nullable();
            $table->string('lng', 100)->nullable();
            $table->unsignedInteger('states_br_id');

            $table->index(["states_br_id"], 'fk_towns_br_states_br1_idx');

            $table->unique(["id"], 'id_UNIQUE');


            $table->foreign('states_br_id', 'fk_towns_br_states_br1_idx')
                ->references('id')->on('states_br')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
