<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('username', 80)->unique();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('company', 254);
            $table->string('phone', 100);
            $table->string('cpf', 20)->nullable()->default(null);
            $table->string('cnpj', 20)->nullable()->default(null);
            $table->string('postal_code', 20);
            $table->string('address', 200);
            $table->integer('town_id', false, true)->length(10);
            $table->integer('state_id', false, true)->length(10);
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('town_id')
                ->references('id')->on('towns_br')
                ->onDelete('no action')
                ->onUpdate('no action');
            
                $table->foreign('state_id')
                ->references('id')->on('states_br')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
