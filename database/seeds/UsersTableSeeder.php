<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new App\User;
        $user->name = 'Administrador';
        $user->username = 'admin';
        $user->email = 'admin@email.com';
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->password = Hash::make('290494');
        $user->company = 'Evolve';
        $user->phone = '82999318021';
        $user->cpf = '09275590486';
        $user->postal_code = '57254000';
        $user->address = 'Rua Santa Luzia, 48';
        $user->town_id = 948;
        $user->state_id = 2;
        $user->save();

        $u = App\User::find($user->id);
        
        $u->groups()->attach(1);

        $user = new App\User;
        $user->name = 'Cliente';
        $user->username = 'client';
        $user->email = 'cliente@email.com';
        $user->email_verified_at = date('Y-m-d H:i:s');
        $user->password = Hash::make('290494');
        $user->company = 'Evolve';
        $user->phone = '82999318021';
        $user->cpf = '09275590486';
        $user->postal_code = '57254000';
        $user->address = 'Rua Santa Luzia, 48';
        $user->town_id = 948;
        $user->state_id = 2;
        $user->save();

        $u = App\User::find($user->id);
        
        $u->groups()->attach(2);
    }
}
