<?php

use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (['Admin', 'Cliente'] as $groupName) {

            $group = new App\Group;
            
            $group->name = $groupName;
            $group->save();

            if ($groupName == 'Admin') {

                $permissions = App\Permission::all();
        
                $g = App\Group::find($group->id);
        
                foreach ($permissions as $perm) {
                    $g->permissions()->attach($perm->id);
                }
            }
    
    
            $this->command->info('Grupo: '.$group->name.' adicionado.');
        }
    }
}
