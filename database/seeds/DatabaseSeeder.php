<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Estados
        $this->call(TableStatesBrSeeder::class);
        
        // Cidades
        $this->call(TableTownsBrSeeder::class);
        
        //Permissões
        $this->call(PermissionsTableSeeder::class);
        
        //Grupos
        $this->call(GroupsTableSeeder::class);

        //Usuários
        $this->call(UsersTableSeeder::class);
    }
}
