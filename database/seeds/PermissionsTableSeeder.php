<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perm = [
            'permissions' => '@Permissões',
            'users'       => 'Usuários'
            // add as permissões aqui
        ];

        $suffix = [
            '_all' => 'Listar',
            '_add' => 'Adicionar',
            '_view' => 'Visualizar',
            '_edit' => 'Editar',
            '_remove' => 'Remover'
        ];

        foreach ($perm as $key => $value) {
            $public = 1;

            if (strpos($value, '@') !== false) {
                $public = 0;
                $value = str_replace('@', '', $value);
            }

            foreach ($suffix as $skey => $svalue) {
                $name = $value.' - '.$svalue;
                $route = $key.'/';
                $tag = $key.$skey;

                
                $exist = App\Permission::where('tag', '=', $tag)->first();
                
                if (!$exist) {
                    $p = new App\Permission;
                } else {
                    $p = $exist;
                }
                
                $p->name = $name;
                $p->route = $route;
                $p->tag = $tag;
                $p->public = $public;
                
                $p->save();
                
                $this->command->info('Adicionada Permissão: '. $name);
            }
        }
    }
}
