<?php

use Illuminate\Database\Seeder;

class TableStatesBrSeeder extends Seeder
{
    public $set_schema_table = 'states_br';
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table($this->set_schema_table)->insert([
            ['state' => 'Acre', 'initial' => 'AC'],
            ['state' => 'Alagoas', 'initial' => 'AL'],
            ['state' => 'Amapá', 'initial' => 'AP'],
            ['state' => 'Amazonas', 'initial' => 'AM'],
            ['state' => 'Bahia', 'initial' => 'BA'],
            ['state' => 'Ceará', 'initial' => 'CE'],
            ['state' => 'Distrito Federal', 'initial' => 'DF'],
            ['state' => 'Espírito Santo', 'initial' => 'ES'],
            ['state' => 'Goiás', 'initial' => 'GO'],
            ['state' => 'Maranhão', 'initial' => 'MA'],
            ['state' => 'Mato Grosso', 'initial' => 'MT'],
            ['state' => 'Mato Grosso do Sul', 'initial' => 'MS'],
            ['state' => 'Minas Gerais', 'initial' => 'MG'],
            ['state' => 'Pará', 'initial' => 'PA'],
            ['state' => 'Paraíba', 'initial' => 'PB'],
            ['state' => 'Paraná', 'initial' => 'PR'],
            ['state' => 'Pernambuco', 'initial' => 'PE'],
            ['state' => 'Piauí', 'initial' => 'PI'],
            ['state' => 'Rio de Janeiro', 'initial' => 'RJ'],
            ['state' => 'Rio Grande do Norte', 'initial' => 'RN'],
            ['state' => 'Rio Grande do Sul', 'initial' => 'RS'],
            ['state' => 'Rondônia', 'initial' => 'RO'],
            ['state' => 'Roraima', 'initial' => 'RR'],
            ['state' => 'Santa Catarina', 'initial' => 'SC'],
            ['state' => 'São Paulo', 'initial' => 'SP'],
            ['state' => 'Sergipe', 'initial' => 'SE'],
            ['state' => 'Tocantins', 'initial' => 'TO']
        ]);

        $this->command->info('Estados Brasileiros importados com sucesso!');
    }
}
