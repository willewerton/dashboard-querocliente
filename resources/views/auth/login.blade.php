<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <title>Quero Cliente - Logar</title>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Lato:400,400i,700,900">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>

    <div class="page-login page-login-body">
        <div class="container page-login__container">
            <div class="page-login-logo">
                <img src="assets/images/logo.png" alt="Logo Quero Cliente">
            </div>
            <!-- /.page-login-logo -->

            <div class="page-login-box">
                <div class="page-login-box__title">
                    Faça Login:
                </div>
                <!-- /.page-login-form-box__title -->

                <div class="page-login-box__form">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="email" placeholder="E-mail:" value="{{ old('email') }}" class="form-control"
                                required>
                            <!-- /.form-control -->
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <!-- /.form-group -->

                        <div class="form-group">
                            <input type="password" name="password" placeholder="Senha:" class="form-control" required>
                            <!-- /.form-control -->
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <!-- /.form-group -->

                        @if (Route::has('password.request'))
                        <div class="form-group page-login-box__link">
                            <a href="{{ route('password.request') }}">Esqueceu a senha?</a>
                        </div>
                        @endif

                        <!-- /.page-login-box__link -->

                        <div class="form-group">
                            <button type="submit" class="btn btn--big btn--block">Acessar minha máquina</button>
                        </div>
                        <!-- /.form-group -->
                    </form>
                </div>
                <!-- /.page-login-box__form -->
            </div>
            <!-- /.page-login-form-box -->

            <div class="page-login-logo-onemedia">
                <a href="http://onelifemedia.com.br" target="_blank" title="One Life Media"><img
                        src="assets/images/one-life.png" alt="Logo One Life Media"></a>
            </div>
            <!-- /.page-login-logo-onemedia -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.page-login-body -->

    <footer class="footer footer--fixed">
        <div class="container">
            <div class="footer-dev">
                © {{ date( 'Y' ) }} - <span>Quero Cliente</span> - Todos os direitos reservados
            </div>
            <!-- /.footer-dev -->
        </div>
        <!-- /.container -->
    </footer>
    <!-- /.footer -->

</body>

</html>