<?php

return [

    /*
    |--------------------------------------------------------------------------
    | textos
    |--------------------------------------------------------------------------
    |
    | Textos usados no site
    | 
    |
    */

    'version' => '?v=1.8',

    'errors'             =>  [
        'exception'      => 'Ocorreu um erro ao tentar processar a solicitação',
        'fields'         => 'Preencha todos campos obrigatórios.',

        'image'          => [
            'size'       => 'A imagem excede o tamanho máximo',
            'extension'  => 'Formato de imagem inválido'
        ],
    ]     
    

];
