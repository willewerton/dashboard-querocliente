<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'active'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];


    /**
     * Relacionamento many to may com a tabela de permissões
     */
    public function permissions () {
        return $this->belongsToMany('App\Permission', 'group_permissions', 'group_id', 'permission_id');
    }
    
    /**
     * Relacionamento many to may com a tabela de usuários
     */
    public function users () {
        return $this->belongsToMany('App\User', 'user_groups', 'group_id', 'user_id');
    }

}
