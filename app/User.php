<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Permission;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Relacionamento many to may com a tabela de grupos
     */
    public function groups () {
        return $this->belongsToMany('App\Group', 'user_groups', 'user_id', 'group_id');
    }

    public function hasPermission(Permission $permission) {
        return $this->hasAnyGroup($permission->groups);
    }

    public function hasAnyGroup ($groups) {
        if(is_array($groups) || is_object($groups) ) {
            return !! $groups->intersect($this->groups)->count();
        }
        
        return $this->groups->contains('name', $groups);
    }
}
